﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{

    /// <summary>
    /// Classe du Model de la table ENREG 
    /// </summary>
    public class ENREG
    {
        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Key]
        [Column("EN_CODE")]
        public int EN_CODE { get; set; }

        /// <summary>
        /// Objet
        /// </summary>
        [Column("EN_OBJET")]
        public string EN_OBJET { get; set; }

        /// <summary>
        /// Commentaire associé 
        /// </summary>
        [Column("EN_COMMENTAIRE")]
        public string EN_COMMENTAIRE { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        [Column("EN_DATE")]
        public DateTime EN_DATE { get; set; }

        /// <summary>
        /// Temps
        /// </summary>
        //[Column("EN_TEMPS")]
        //public TimeSpan EN_TEMPS { get; set; }

        /// <summary>
        /// code de Taches associé
        /// </summary>
        [ForeignKey("TACHE")]
        [Column("TAC_CODE")]
        public int TAC_CODE { get; set; }

        /// <summary>
        /// Identifiant utilisateur assocé a ça
        /// </summary>
        [Column("UTI_CODE")]
        public int UTI_CODE { get; set; }

        /// <summary>
        /// Date de Modification
        /// </summary>
        [Column("EN_DATE_MODIF")]
        public string EN_DATE_MODIF { get; set; }

    }
}
