﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{
    /// <summary>
    /// classe de description de projet
    /// </summary>
    public class PROJET
    {
        /// <summary>
        /// Identifiant du Projet
        /// </summary>
        [Key]
        [Column("PRO_CODE")]
        public int PRO_CODE { get; set; }

        /// <summary>
        /// Nom du projet
        /// </summary>
        [Column("PRO_NOM")]
        public string PRO_NOM { get; set; }

        /// <summary>
        /// Statu du projet
        /// </summary>
        [Column("PRO_STATUT")]
        public string PRO_STATUT { get; set; }

        /// <summary>
        /// Description du projet
        /// </summary>
        [Column("PRO_DESCRIPTION")]
        public string PRO_DESCRIPTION { get; set; }

        /// <summary>
        /// ?
        /// </summary>
        [Column("PRO_PONC_MAIN")]
        public bool PRO_PONC_MAIN { get; set; }

        /// <summary>
        /// Identifiant du sondage
        /// </summary>
        [Column("PRO_JOUR_PREVU")]
        public string PRO_JOUR_PREVU { get; set; }

        /// <summary>
        /// date de début du projet
        /// </summary>
        [Column("PRO_DATE_DEBUT")]
        public DateTime? PRO_DATE_DEBUT { get; set; }

        /// <summary>
        /// la date de fin du projet
        /// </summary>
        [Column("PRO_DATE_FIN")]
        public DateTime? PRO_DATE_FIN { get; set; }

        /// <summary>
        /// Date ciblé par le projet
        /// </summary>
        [Column("PRO_DATE_CIBLE")]
        public DateTime? PRO_DATE_CIBLE { get; set; }

        /// <summary>
        /// Identifiant du client associé au projet
        /// </summary>
        [ForeignKey("CLIENT")]
        [Column("CLI_CODE")]
        public int? CLI_CODE { get; set; }

        /// <summary>
        /// Identifiant du contacte associé au projet
        /// </summary>
        [ForeignKey("CONTACT")]
        [Column("CON_CODE")]
        public int? CON_CODE { get; set; }

        /// <summary>
        /// Identifiant du sondage
        /// </summary>
        [Column("PRO_DATE_MODIF")]
        public string PRO_DATE_MODIF { get; set; }
    }
}
