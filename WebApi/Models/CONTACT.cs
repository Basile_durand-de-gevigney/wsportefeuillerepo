﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{
    public class CONTACT
    {
        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Key]
        [Column("CON_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CON_PRENOM")]
        public string Prenom { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CON_NOM")]
        public string Nom { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CON_EMAIL")]
        public string Email { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CON_NUMERO")]
        public string Numero { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CLI_CODE")]
        public int Client { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CON_IMPORTANT")]
        public bool Important { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CON_DATE_MODIF")]
        public string DateDeModification { get; set; }

    }
}
