﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{

    [Table("UTILISATEUR")]
    public partial class UTILISATEUR
    {
        [Key]
        [Column("UTI_ID")]
        public string UTI_ID { get; set; }

        [Column("UTI_PRENOM")]
        public string UTI_PRENOM { get; set; }

        [Column("UTI_CODE")]
        public int UTI_CODE { get; set; }

        [Column("UTI_NOM")]
        public string UTI_NOM { get; set; }

        [Column("UTI_EMAIL")]
        public string UTI_EMAIL { get; set; }

        [Column("UTI_GROUPE")]
        public string UTI_GROUPE { get; set; }

        [Column("UTI_MDP")]
        public string UTI_MDP { get; set; }

        [Column("UTI_DATE_MODIF")]
        public string UTI_DATE_MODIF { get; set; }

        [Column("UTI_STATUT")]
        public string UTI_STATUT { get; set; }
    }
}