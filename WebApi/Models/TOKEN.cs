﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{
    /// <summary>
    /// definition de la table token 
    /// </summary>
    [Table("TOKEN")]
    public class TOKEN
    {
        /// <summary>
        /// Identifiant utilisateur
        /// </summary>
        [Column("TOK_UTILISATEUR_ID")]
        public string User_Id { get; set; }

        /// <summary>
        /// identifiant du token devras etre transmi a l'utilisateur qui a sont tour devra l'utiliser
        /// </summary>
        [Key]
        [Column("TOK_TOKEN")]
        public string Token { get; set; }
        /// <summary>
        /// Date de fin du token
        /// </summary>
        /// <format>yyyy-MM-dd HH:mm:ss</format>
        /// <remarks>on ne prend pas la date de début </remarks>
        [Column("TOK_DATE_PERIME")]
        public DateTime DatePerime { get; set; }
    }
}