﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{
    public class CLIENT
    {
        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Key]
        [Column("CLI_CODE")]
        public int CLI_CODE { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CLI_SOCIETE")]
        public string CLI_SOCIETE { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CLI_STATUT")]
        public string CLI_STATUT { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CLI_VILLE")]
        public string CLI_VILLE { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CLI_ADRESSE")]
        public string CLI_ADRESSE { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CLI_CODE_POSTAL")]
        public string CLI_CODE_POSTAL { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CLI_CREDIT")]
        public int? CLI_CREDIT { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CLI_ANNEE")]
        public int? CLI_ANNEE { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("CLI_DATE_MODIF")]
        public string CLI_DATE_MODIF { get; set; }
    }
}
