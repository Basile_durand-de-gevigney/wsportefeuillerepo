﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{
    /// <summary>
    /// definition de la table de TACHE 
    /// </summary>
    [Table("TACHE")]
    public partial class TACHE
    {
        /// <summary>
        /// Identifiant de la tache
        /// </summary>
        [Key]
        [Column("TAC_CODE")]
        public int TAC_CODE { get; set; }

        /// <summary>
        /// Nom de la tache
        /// </summary>
        [Column("TAC_NOM")]
        public string TAC_NOM { get; set; }

        /// <summary>
        /// statu de la tache
        /// </summary>
        [Column("TAC_STATUT")]
        public string TAC_STATUT { get; set; }

        /// <summary>
        /// temps prévu en heurs
        /// </summary>
        [Column("TAC_HEURE_PREVU")]
        public string TAC_HEURE_PREVU { get; set; }

        /// <summary>
        /// la personne assigne a la tache
        /// </summary>
        [Column("TAC_PERSONNE_ASSIGNE")]
        public string TAC_PERSONNE_ASSIGNE { get; set; }

        /// <summary>
        /// date cible de la tache
        /// </summary>
        [Column("TAC_DATE_CIBLE")]
        public DateTime? TAC_DATE_CIBLE { get; set; }

        /// <summary>
        /// description de la tache
        /// </summary>
        [Column("TAC_DESCRIPTION")]
        public string TAC_DESCRIPTION { get; set; }

        /// <summary>
        /// Identifiant du projet / associé a la tache
        /// </summary>
        [ForeignKey("PROJET")]
        [Column("PRO_CODE")]
        public int PRO_CODE { get; set; }

        /// <summary>
        /// dernierre modification
        /// </summary>
        [Column("TAC_DATE_MODIF")]
        public string TAC_DATE_MODIF { get; set; }

    }
}
