﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{
    public class EMAIL
    {
        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Key]
        [Column("EMA_CODE")]
        public int Code { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("EMA_DESTINATAIRE")]
        public string Destinataire { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("EMA_DCOPY")]
        public string DestinatairesCopy { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("EMA_OBJET")]
        public string Objet { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("EMA_CORPS")]
        public string Corps { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("EMA_DATE")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [ForeignKey("ENREG")]
        [Column("EN_CODE")]
        public int CodedeENREG { get; set; }

        /// <summary>
        /// Identifiant de la table
        /// </summary>
        [Column("EMA_DATE_MODIF")]
        public string DateDeModification { get; set; }
    }
}
