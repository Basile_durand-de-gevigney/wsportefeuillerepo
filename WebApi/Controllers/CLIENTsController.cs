﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using B3_WsPortefeuille.Bdd;
using B3_WsPortefeuille.Models;
using System.Net;
using System.Text.Json;
using System.Net.Http;

namespace B3_WsPortefeuille.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CLIENTsController : ControllerBase
    {
        // GET: api/CLIENTs
        /// <summary>
        /// renvois tout les enregistement / heures qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<CLIENT>> GetCLIENTs()
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var cLIENT = (from t in db.CLIENT
                                  select t).ToList();
                    if (cLIENT == null)
                    {
                        return NotFound();
                    }
                    return cLIENT;
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // GET: api/CLIENTs/{id}
        /// <summary>
        /// renvois tout les enregistement / heures qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CLIENT>> GetCLIENT(int id)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var eNREG = (from c in db.CLIENT
                                 where c.CLI_CODE == id
                                 select c).ToList().FirstOrDefault();
                    if (eNREG == null)
                    {
                        return NotFound();
                    }
                    return eNREG;
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // POST: api/CLIENTs
        /// <summary>
        /// renvois tout les TACHE qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string PostCLIENT([FromBody] dynamic inClient)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** deserialisarion de la tache
            */
            CLIENT client = null;
            try
            {
                client = JsonSerializer.Deserialize(inClient.ToString(), typeof(CLIENT));
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("La requête a échoué. le JSON envoyé ne correspond pas au type CLIENT, il ne peux donc pas etre déserialisé donc traité."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var a = db.CLIENT.Add(client);
                    db.SaveChanges();
                }
                return "[OK]";
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // GET: api/CLIENTs
        /// <summary>
        /// renvois tout les TACHE qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public string DeletCLIENT(int id)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete on retrouve le client puis on le supprime
            */
            try
            {
                using (var db = new Context())
                {
                    var cLIENT = (from c in db.CLIENT
                                  where c.CLI_CODE == id
                                  select c).ToList().FirstOrDefault();
                    if (cLIENT == null)
                        return "Le CLIENT que vous essayez de supprimer n'existe pas.";

                    db.CLIENT.Remove(cLIENT);
                    db.SaveChanges();
                }
                return "[OK]";
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // PUT: api/CLIENTs
        /// <summary>
        /// Update le client 
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public string PutCLIENT([FromBody] dynamic inClient)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** deserialisarion de la tache
            */
            CLIENT client = null;
            try
            {
                client = JsonSerializer.Deserialize(inClient.ToString(), typeof(CLIENT));
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("La requête a échoué. le JSON envoyé ne correspond pas au type CLIENT, il ne peux donc pas etre déserialisé donc traité."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var existingClient = db.CLIENT.Where(s => s.CLI_CODE == client.CLI_CODE).FirstOrDefault<CLIENT>();

                    existingClient.CLI_ADRESSE = client.CLI_ADRESSE;
                    existingClient.CLI_ANNEE = client.CLI_ANNEE;
                    existingClient.CLI_CODE_POSTAL = client.CLI_CODE_POSTAL;
                    existingClient.CLI_CREDIT = client.CLI_CREDIT;

                    existingClient.CLI_DATE_MODIF = DateTime.Now.ToString("yyyy-MM-dd");
                    existingClient.CLI_SOCIETE = client.CLI_SOCIETE;
                    existingClient.CLI_STATUT = client.CLI_STATUT;
                    existingClient.CLI_VILLE = client.CLI_VILLE;

                    db.SaveChanges();
                }
                return "[OK]";
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }
    }
}
