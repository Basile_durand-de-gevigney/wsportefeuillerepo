﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using B3_WsPortefeuille.Bdd;
using B3_WsPortefeuille.Models;
using System.Net;
using System.Text.Json;
using System.Net.Http;

namespace B3_WsPortefeuille.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TACHEsController : ControllerBase
    {
        // GET: api/TACHEs
        /// <summary>
        /// renvois tout les TACHE qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<TACHE>> GetCLIENTs()
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var tACHE = (from t in db.TACHE
                                 select t).ToList();
                    if (tACHE == null)
                    {
                        return NotFound();
                    }
                    return tACHE;
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // GET: api/TACHEs/{id}
        /// <summary>
        /// renvois tout les TACHE qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<TACHE> GetTACHE(int id)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var tACHE = (from c in db.TACHE
                                 where c.TAC_CODE == id
                                 select c).ToList().FirstOrDefault();
                    if (tACHE == null)
                    {
                        return NotFound();
                    }
                    return tACHE;
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // GET: api/TACHEs
        /// <summary>
        /// renvois tout les TACHE qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string PostTACHE([FromBody] dynamic inTache)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** deserialisarion de la tache
            */
            TACHE Tache = null;
            try
            {
                Tache = JsonSerializer.Deserialize(inTache.ToString(), typeof(TACHE));
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("La requête a échoué. le JSON envoyé ne correspond pas au type CLIENT, il ne peux donc pas etre déserialisé donc traité."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    db.TACHE.Add(Tache);
                }
                return "[OK]";
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // GET: api/TACHEs
        /// <summary>
        /// renvois tout les TACHE qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public string DeletTACHE(int id)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete on retrouve le taches puis on le supprime
            */
            try
            {
                using (var db = new Context())
                {
                    var tACHE = (from t in db.TACHE
                                 where t.TAC_CODE == id
                                 select t).ToList().FirstOrDefault();
                    if (tACHE == null)
                        return "La TACHE que vous essayez de supprimer n'existe pas.";
                    db.TACHE.Remove(tACHE);
                    db.SaveChanges();
                }
                return "[OK]";
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // PUT: api/TACHEs
        /// <summary>
        /// Update le taches 
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public string TACHE([FromBody] dynamic inTache)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** deserialisarion de la tache
            */
            TACHE tache = null;
            try
            {
                tache = JsonSerializer.Deserialize(inTache.ToString(), typeof(TACHE));
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("La requête a échoué. le JSON envoyé ne correspond pas au type CLIENT, il ne peux donc pas etre déserialisé donc traité."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var existingTache = db.TACHE.Where(s => s.TAC_CODE == tache.TAC_CODE).FirstOrDefault<TACHE>();

                    existingTache.TAC_DATE_CIBLE = tache.TAC_DATE_CIBLE;
                    existingTache.TAC_DATE_MODIF = tache.TAC_DATE_MODIF;
                    existingTache.TAC_DESCRIPTION = tache.TAC_DESCRIPTION;
                    existingTache.TAC_HEURE_PREVU = tache.TAC_HEURE_PREVU;

                    existingTache.TAC_NOM = tache.TAC_NOM;
                    existingTache.TAC_PERSONNE_ASSIGNE = tache.TAC_PERSONNE_ASSIGNE;
                    existingTache.TAC_STATUT = tache.TAC_STATUT;

                    existingTache.TAC_DATE_MODIF = DateTime.Now.ToString("yyyy-MM-dd");

                    db.SaveChanges();
                }
                return "[OK]";

            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }
    }
}
