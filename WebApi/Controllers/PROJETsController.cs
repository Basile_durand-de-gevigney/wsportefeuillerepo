﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using B3_WsPortefeuille.Bdd;
using B3_WsPortefeuille.Models;
using System.Net;
using System.Text.Json;
using System.Net.Http;

namespace B3_WsPortefeuille.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PROJETsController : ControllerBase
    {
        // GET: api/PROJETs
        /// <summary>
        /// renvois tout les projets
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<PROJET>> GetPROJETs()
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var pROJET = (from t in db.PROJET
                                  select t).ToList();
                    if (pROJET == null)
                    {
                        return NotFound();
                    }
                    return pROJET;
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // GET: api/PROJETs
        /// <summary>
        /// renvois tout les projets
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<PROJET> GetPROJETs(int id)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var pROJET = (from t in db.PROJET
                                  where t.PRO_CODE == id
                                  select t).ToList().FirstOrDefault();
                    if (pROJET == null)
                    {
                        return NotFound();
                    }
                    return pROJET;
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }


        // POST: api/CLIENTs
        /// <summary>
        /// renvois tout les TACHE qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string PostPROJET([FromBody] dynamic inProjet)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** deserialisarion de la tache
            */
            PROJET projet = null;
            try
            {
                projet = JsonSerializer.Deserialize(inProjet.ToString(), typeof(PROJET));
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("La requête a échoué. le JSON envoyé ne correspond pas au type PROJET, il ne peux donc pas etre déserialisé donc traité."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var a = db.PROJET.Add(projet);
                    db.SaveChanges();
                }
                return "[OK]";
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // GET: api/PROJETs
        /// <summary>
        /// renvois tout les TACHE qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public string DeletPROJET(int id)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete on retrouve le projet puis on le supprime
            */
            try
            {
                using (var db = new Context())
                {
                    var pROJET = (from c in db.PROJET
                                  where c.CLI_CODE == id
                                  select c).ToList().FirstOrDefault();
                    if (pROJET == null)
                        return "Le PROJET que vous essayez de supprimer n'existe pas.";
                    db.PROJET.Remove(pROJET);
                    db.SaveChanges();
                }
                return "[OK]";
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // PUT: api/PROJETs
        /// <summary>
        /// Update le projet 
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public string PROJET([FromBody] dynamic inProjet)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** deserialisarion de la tache
            */
            PROJET projet = null;
            try
            {
                projet = JsonSerializer.Deserialize(inProjet.ToString(), typeof(PROJET));
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("La requête a échoué. le JSON envoyé ne correspond pas au type PROJET, il ne peux donc pas etre déserialisé donc traité."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
            /*
            ** requete 
            */

            try
            {
                using (var db = new Context())
                {
                    var existingProjet = db.PROJET.Where(s => s.PRO_CODE == projet.PRO_CODE).FirstOrDefault<PROJET>();

                    existingProjet.PRO_DATE_CIBLE = projet.PRO_DATE_CIBLE;
                    existingProjet.PRO_DATE_DEBUT = projet.PRO_DATE_DEBUT;
                    existingProjet.PRO_DATE_FIN = projet.PRO_DATE_FIN;
                    existingProjet.PRO_DESCRIPTION = projet.PRO_DESCRIPTION;

                    existingProjet.PRO_JOUR_PREVU = projet.PRO_JOUR_PREVU;
                    existingProjet.PRO_NOM = projet.PRO_NOM;
                    existingProjet.PRO_PONC_MAIN = projet.PRO_PONC_MAIN;
                    existingProjet.PRO_STATUT = projet.PRO_STATUT;

                    existingProjet.PRO_DATE_MODIF = DateTime.Now.ToString("yyyy-MM-dd");


                    db.SaveChanges();
                }
                return "[OK]";
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }
    }
}
