﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using B3_WsPortefeuille.Bdd;
using B3_WsPortefeuille.Models;
using System.Net;
using System.Net.Http;

namespace B3_WsPortefeuille.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UTILISATEURsController : ControllerBase
    {

        // GET: api/UTILISATEURs
        [HttpGet]
        public ActionResult<IEnumerable<UTILISATEUR>> GetUTILISATEUR()
        {
            /*
            ** verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var uTILISATEUR = (from t in db.UTILISATEUR
                                       select t).ToList();

                    if (uTILISATEUR == null)
                    {
                        return NotFound();
                    }
                    foreach (var u in uTILISATEUR)
                    {
                        u.UTI_MDP = "***";
                    }
                    return uTILISATEUR;
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // GET: api/UTILISATEURs/{id_utilisateur}
        [HttpGet("{id}")]
        public async Task<ActionResult<UTILISATEUR>> GetUTILISATEUR(string id)
        {
            /*
            ** verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var uTILISATEUR = (from t in db.UTILISATEUR
                                       where t.UTI_ID == id
                                       select t).ToList().FirstOrDefault();
                    if (uTILISATEUR == null)
                    {
                        return NotFound();
                    }
                    uTILISATEUR.UTI_MDP = "***";
                    return uTILISATEUR;
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }
    }
}
