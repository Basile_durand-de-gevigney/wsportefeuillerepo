﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using B3_WsPortefeuille.Bdd;
using B3_WsPortefeuille.Models;
using System.Net;
using System.Text.Json;
using System.Net.Http;

namespace B3_WsPortefeuille.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ENREGsController : ControllerBase
    {

        // GET: api/ENREGs
        /// <summary>
        /// renvois tout les enregistement / heures qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<ENREG>> GetENREGs()
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var eNREGs = (from t in db.ENREG
                                  select t).ToList();
                    if (eNREGs == null)
                    {
                        return NotFound();
                    }
                    return eNREGs;
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // GET: api/ENREGs/{id}
        /// <summary>
        /// renvois tout les enregistement / heures qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<ENREG> GetENREGs(int id)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var eNREG = (from t in db.ENREG
                                 where t.EN_CODE == id
                                 select t).ToList().FirstOrDefault();
                    if (eNREG == null)
                    {
                        return NotFound();
                    }
                    return eNREG;
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // POST: api/CLIENTs
        /// <summary>
        /// renvois tout les TACHE qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string PostENREG([FromBody] dynamic inEnreg)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** deserialisarion de la tache
            */
            ENREG enreg = null;
            try
            {
                enreg = JsonSerializer.Deserialize(inEnreg.ToString(), typeof(ENREG));
            }
            catch
            {
            }
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var a = db.ENREG.Add(enreg);
                    db.SaveChanges();
                }
                return "[OK]";
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // GET: api/ENREGs
        /// <summary>
        /// renvois tout les TACHE qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public string DeletENREG(int id)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);

            /*
            ** requete on retrouve le enreg puis on le supprime
            */
            try
            {
                using (var db = new Context())
                {
                    var eNREG = (from e in db.ENREG
                                 where e.EN_CODE == id
                                 select e).FirstOrDefault();
                    if (eNREG == null)
                        return "L'ENREGISTREMENT que vous essayez de supprimer n'existe pas.";
                    db.ENREG.Remove(eNREG);
                    db.SaveChanges();
                }
                return "[OK]";
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }

        // PUT: api/ENREGs
        /// <summary>
        /// Update le enreg 
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public string ENREG([FromBody] dynamic inEnreg)
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);

            /*
            ** deserialisarion de la tache
            */
            ENREG enreg = null;
            try
            {
                enreg = JsonSerializer.Deserialize(inEnreg.ToString(), typeof(ENREG));
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("la Deserialization de l'objet envoyer est impossible")),
                    ReasonPhrase = "Bad JSON, Deserialize error"
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }

            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var existingEnreg = db.ENREG.Where(s => s.EN_CODE == enreg.EN_CODE).FirstOrDefault<ENREG>();

                    existingEnreg.EN_COMMENTAIRE = enreg.EN_COMMENTAIRE;
                    existingEnreg.EN_DATE = enreg.EN_DATE;
                    existingEnreg.EN_OBJET = enreg.EN_OBJET;

                    existingEnreg.EN_DATE_MODIF = DateTime.Now.ToString("yyyy-MM-dd");

                    db.SaveChanges();
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
            return "[OK]";
        }

    }
}
