﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using B3_WsPortefeuille.Bdd;
using B3_WsPortefeuille.Models;
using System.Net;
using System.Net.Http;

namespace B3_WsPortefeuille.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MESENREGsController : ControllerBase
    {
        
        // GET: api/MESENREGs
        /// <summary>
        /// renvois tout les enregistement / heures qui concerne l'utilisateur 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<ENREG>> GetENREGs()
        {
            /* verification du token 
            */
            TOKEN token;
            if (this.Request.Headers.TryGetValue("Authorization", out var val))
            {
                if ((token = Autentification.Verif(val)) == null)
                    throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            }
            else
                throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
            /*
            ** requete 
            */
            try
            {
                using (var db = new Context())
                {
                    var eNREG = (from t in db.ENREG
                                 join u in db.UTILISATEUR on t.UTI_CODE equals u.UTI_CODE
                                 where u.UTI_ID == token.User_Id
                                 select t).ToList();
                    if (eNREG == null)
                    {
                        return NotFound();
                    }
                    return eNREG;
                }
            }
            catch
            {
                /*
                ** gestion d'erreur
                */
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent(string.Format("La requête a échoué. Cela peut provenir d’un conflit avec la base de données ou elle peut ne pas avoir répondue."))
                };
                throw new System.Web.Http.HttpResponseException(resp);
            }
        }
    }
}
