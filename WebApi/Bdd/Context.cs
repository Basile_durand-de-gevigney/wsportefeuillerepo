﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using WebApi.Models;

namespace B3_WsPortefeuille.Bdd
{
    /// <summary>
    /// context représantant la base de donnée
    /// </summary>
    public class Context : DbContext
    {
        public DbSet<PROJET> PROJET { get; set; }
        public DbSet<ENREG> ENREG { get; set; }
        public DbSet<CLIENT> CLIENT { get; set; }
        public DbSet<TOKEN> TOKEN { get; set; }
        public DbSet<UTILISATEUR> UTILISATEUR { get; set; }
        public DbSet<TACHE> TACHE { get; set; }
    }
}
